###Lists

mainList = ["Peter", "Cassey"]

# print(id(mainList))

# anotherList = mainList
# print(id(anotherList))
# anotherList[1] = "Alice"
# print(id(anotherList))


# print(mainList)


###Variables

# x = 5
# print(id(x))
# y = x
# print(id(y))
# y = 6
# print(id(y))

# print("x is:", x)
# print("y is:", y)

###Dictionaries

# d = {"Poland" : "Warsaw"}

# print(id(d))

# anotherDict = d
# print(id(anotherDict))

# d["Poland"] = "Berlin"
# print(id(anotherDict))


