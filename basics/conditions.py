print("Provide some number:")
someNumber = input()
someNumber = int(someNumber)
if someNumber > 0:
    print("The provided number is positive.")
elif someNumber < 0:
    print("The provided number is negative.")
else:
    print("The provided number equals zero.")
