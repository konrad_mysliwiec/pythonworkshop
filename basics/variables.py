##Integer
i = 10
print("The value of i is:", i)

##Float
f = 10
print("The value of f is:", f)

#Bool
b = True
print("The value of b is:", b)

#None
n = None
print("The value of n is:", n)