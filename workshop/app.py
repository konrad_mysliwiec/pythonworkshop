from flask import Flask
import scrabbleSearch as scrabble

app = Flask(__name__)

@app.route("/")
def index():
    return "Hello GSK!"

@app.route("/<string:letters>")
def hello(letters):
    string = scrabble.returnNFirstItems(scrabble.findWords(letters))
    # string = "<p>ppp<p>ooo"
    return string
