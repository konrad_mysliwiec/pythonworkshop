import operator
import re
import itertools
import collections

vocabularyFile = open("collins_scrabble_words.txt", 'r')

############################
######Data check
############################

# # print(vocabularyFile.read())

# # vocabulary = vocabularyFile.read()

# # counter = 1

# for line in vocabularyFile:
#     print(counter, " ", line)
#     counter = counter + 1





############################
######Working on sets and arrays
############################

# ##For loop

#Names which we are looking for
# searchedNamesArray = ['bob', 'john', 'mike']

#List of names
# namesArray = ["bob", "john", "mike", "p"]

#Check if names are in array

# print("bob" in namesArray)

#Array
# for name in searchedNamesArray:
#     print(name in namesArray)


# # isInNames = True

# # for name in names:
# #     isInNames = isInNames and name in ["bob", "john", "mike", "p"]
# #     # isInNames = isInNames and name in ["p", "john", "mike", "p"]

##One liner

# # print(all(st in ["bob", "john", "mike", "p"] for st in names))

# # print(isInNames)

### For loop in sentence


## 1

# word = "abdominal"
# letters = ["d", "d"]
# lettersCounter = 0

# for letter in letters:
#     isLetterInWord = letter in word
#     if isLetterInWord:
#         lettersCounter = lettersCounter + 1
#         # word = word.replace(letter, "", 1)

# print(lettersCounter)


## 2

# isAnyLetterInWord = False         #####   ADDED #2

# word = "abdominal"
# letters = ["a", "d"]

# lettersCounter = 0                #####   ADDED #2
# isInVocabularyDict = {}           #####   ADDED #2

# for letter in letters:
#     isAnyLetterInWord = isAnyLetterInWord or letter in word       #####   ADDED #2
#     isLetterInWord = letter in word
#     if isLetterInWord:
#         lettersCounter = lettersCounter + 1
#         word = word.replace(letter, "", 1)
# if isAnyLetterInWord:                                              #####   ADDED #2
#     isInVocabularyDict[word] = lettersCounter                      #####   ADDED #2

# print(isInVocabularyDict)



# ## 3

def doesContainLetters(word: str, letters: list):                  #####   ADDED #3

    isAnyLetterInWord = False

# word = "abdominal"                                                #####  COMMENTED #3
# letters = ["a", "d"]                                              #####  COMMENTED #3

    lettersCounter = 0
#    isInVocabularyDict = {}                                       #####  COMMENTED #3
    currentWord = word                                             #####   ADDED #3

    for letter in letters:
        isAnyLetterInWord = isAnyLetterInWord or letter in currentWord
        isLetterInWord = letter in currentWord                     #####   ADDED #3
        if isLetterInWord:
            lettersCounter = lettersCounter + 1
            currentWord = currentWord.replace(letter, "", 1)       #####   MODIFIED #3
            
    if isAnyLetterInWord:
        # isInVocabularyDict[word] = lettersCounter                #####   COMMENTED #3
        return (word, currentWord)                              #####   ADDED #3
    else:                                                          #####   ADDED #3
        return None                                                #####   ADDED #3


# word = "abdominal"                                                 #####   ADDED #3
# # # letters = ["a", "d"]
# letters = "aa"                                                     #####   ADDED #3

# print(doesContainLetters(word, letters))                           #####   ADDED #3


# # 4

def findWords(letters: list):

    isInVocabularyDict = {}

    for word in vocabularyFile:
        word = word.lower().rstrip("\n")
        wordEntity = doesContainLetters(word, letters)
        if wordEntity != None:
            word, lettersCounter = wordEntity
            isInVocabularyDict[word] = lettersCounter
    # return isInVocabularyDict
    filteredDictionary = {k: v for k, v in isInVocabularyDict.items() if len(v) < 1}
    return filteredDictionary
    # return collections.OrderedDict(sorted(isInVocabularyDict.items(), key = operator.itemgetter(1), reverse = True))

# print(str.count("p", "p"))

# print(findWords("popoos"))
# print(sorted(findWords("piue").items(), key = operator.itemgetter(1), reverse = True))

# print(itertools.islice(findWords("piue").items(), 0, 10))

def printNFirstItems(dictionary: dict):
    dictionary = itertools.islice(dictionary.items(), 0, 10)
    for key, value in dictionary:
        print(key, value)

def returnNFirstItems(dictionary: dict):
    dictionary = itertools.islice(dictionary.items(), 0, 10)
    nFirstItems = ""
    for key, value in dictionary:
        # nFirstItems = nFirstItems + "<p>" + key + " : " + str(value) + " : " + str(len(value))
        nFirstItems = nFirstItems + "\n" + key + " : '" + str(value) + "' : " + str(len(value))
    return(nFirstItems)


# printNFirstItems(findWords("prqsugh"))

print(returnNFirstItems(findWords("prqsugh")))

# if __name__ == "__main__":
#     print(5)