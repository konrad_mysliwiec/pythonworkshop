from flask import Flask

app = Flask(__name__)

@app.route("/")
def index():
    return "Hello GSK!"

@app.route("/")
def hello(name):
    string = "Hello " + str(name)
    return string
